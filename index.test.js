const animateParticles = require('./index.js')

test('should throw with invalid arguments', () => {
  expect(() => animateParticles()).toThrow()
  expect(() => animateParticles(1)).toThrow()
  expect(() => animateParticles(1, {})).toThrow()
  expect(() => animateParticles('1', [])).toThrow()
})

test('should render correctly', () => {
  expect(animateParticles(1, 'L')).toEqual(['X', '.'])
  expect(animateParticles(1, 'LR')).toEqual(['XX', '..'])
  expect(animateParticles(1, 'LL')).toEqual(['XX', 'X.', '..'])
  expect(animateParticles(1, 'LRRL.LR.LRR.R.LRRL.')).toEqual([
    'XXXX.XX.XXX.X.XXXX.',
    '..XXX..X..XX.X..XX.',
    '.X.XX.X.X..XX.XX.XX',
    'X.X.XX...X.XXXXX..X',
    '.X..XXX...X..XX.X..',
    'X..X..XX.X.XX.XX.X.',
    '..X....XX..XX..XX.X',
    '.X.....XXXX..X..XX.',
    'X.....X..XX...X..XX',
    '.....X..X.XX...X..X',
    '....X..X...XX...X..',
    '...X..X.....XX...X.',
    '..X..X.......XX...X',
    '.X..X.........XX...',
    'X..X...........XX..',
    '..X.............XX.',
    '.X...............XX',
    'X.................X',
    '...................',
  ])
})
