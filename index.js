function getInitialState(directions) {
  return directions.reduce(
    (acc, cur, index) => {
      const isDirection = cur !== '.'
      const state = {
        ...acc,
        string: [...acc.string, isDirection ? 'X' : cur],
      }

      if (isDirection) {
        state[cur] = [...acc[cur], index]
      }

      return state
    },
    {
      string: [],
      L: [],
      R: [],
    },
  )
}

function moveParticles(speed, time, state) {
  const steps = speed * time
  const nextState = {
    L: state.L.map(index => index + steps * -1),
    R: state.R.map(index => index + steps),
    string: state.string.map(() => '.'),
  }

  const indexes = [...nextState.L, ...nextState.R]
  nextState.string = indexes.reduce((acc, cur) => {
    if (cur >= 0 && cur < nextState.string.length) {
      acc[cur] = 'X'
    }
    return acc
  }, nextState.string)
  return nextState
}

function animateParticles(speed, directions) {
  if (typeof speed !== 'number') {
    throw new Error('Invalid arguments provided: speed should be a number')
  }

  if (typeof directions !== 'string') {
    throw new Error('Invalid arguments provided: directions should be string')
  }

  const directionsInArray = directions.split('')
  const initialState = getInitialState(directionsInArray)
  const states = [initialState.string]
  let time = 1

  while (states[states.length - 1].includes('X')) {
    const nextState = moveParticles(speed, time, initialState)
    states.push(nextState.string)
    time += 1
  }

  return states.map(state => state.join(''))
}

// console.log(animateParticles(1, 'LRRL.LR.LRR.R.LRRL.'))

// console.time('TIME')
// for (let index = 0; index < 1000; index += 1) {
//   animateParticles(1, 'LRRL.LR.LRR.R.LRRL.')
// }
// console.timeEnd('TIME')

module.exports = animateParticles
